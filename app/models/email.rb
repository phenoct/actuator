class Email < ApplicationRecord
	belongs_to :item

	def send_success_msg
		UserMailer.success_email(self).deliver_later unless self.sent_success_msg 
		self.update(sent_success_msg: true)
	end
end
