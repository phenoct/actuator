require 'digest'
class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :check_edit_authorization, only: [:edit, :update]

  # GET /items
  # GET /items.json
  def index
    @items = Item.where(public: true).order('created_at DESC')
  end

  # GET /items/1
  # GET /items/1.json
  def show
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  def sign_up
    @email = Email.new({email: params[:email], 
                        validated: false,
                        validation_token: SecureRandom.hex,
                        item_id: params[:id]
                        })
    respond_to do |format|
      if @email.save
        UserMailer.validation_email(@email).deliver_later
        format.html { redirect_to item_url(params[:id]), notice: "Signed up - validate your email"}
      else
        format.html { redirect_to item_url(params[:id]), notice: 'Error' }
      end
    end
  end

  def input_password
    password_hash = Item.hash(params[:password])
    @item = Item.find(params[:id])
    if Item.hash(params[:password]) == @item.password_hash
      session[:current_edit] = @item.id
      redirect_to edit_item_url(params[:id])
    else
      redirect_to item_url(params[:id]), notice: 'Wrong password'
    end
  end

  # POST /items
  # POST /items.json
  def create
    item_params = item_create_params
    item_params[:password_hash] = Item.hash(params[:item][:password])
    p item_params
    @item = Item.new(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_update_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    def item_update_params
      params.require(:item).permit(:success_msg, :public, :show_threshold, :show_verified_signups)
    end

    def item_create_params
      params.require(:item).permit(:title, :description, :threshold, :success_msg, :public, :show_threshold, :show_verified_signups)
    end

    def check_edit_authorization
      if !session[:current_edit] || session[:current_edit] != params[:id].to_i
        redirect_to item_url(params[:id]), notice: "You aren't authorized to edit this item"
      end
    end
end
