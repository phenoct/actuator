class EmailsController < ApplicationController
	def validate
		email = Email.find(params[:id])
		if email.validation_token == params[:validation_token]
			email.validated = true
			email.save!
		end

		item = email.item 
		item.succeeded?
		redirect_to item_url(item), notice: "Verified"
	end
end